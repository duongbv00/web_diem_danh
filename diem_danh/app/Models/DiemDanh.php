<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DiemDanh extends Model
{
    protected $table = 'diem_danh';
    protected $fillable = [
    	'ma_giao_vien',
    	'ma_lop',
    	'ma_mon_hoc',
    	'ngay_diem_danh',
    	'thoi_gian_bat_dau',
    	'thoi_gian_ket_thuc'
    ];
    public $timestamps = false;
    protected $primaryKey = 'ma';
}
