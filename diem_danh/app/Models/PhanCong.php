<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Model\Traits\HasCompositePrimaryKey;

class PhanCong extends Model
{
	use Traits\HasCompositePrimaryKey;
    protected $table = 'phan_cong_day';
    protected $fillable = [
    	'ma_lop',
    	'ma_mon_hoc',
    	'ma_giao_vien',
    ];
    public $timestamps = false;
    protected $primaryKey = ['ma_lop','ma_mon_hoc'];

    public function lop()
    {
    	return $this -> belongsTo('App\Models\Lop','ma_lop');
    }
    public function mon_hoc()
    {
    	return $this -> belongsTo('App\Models\MonHoc','ma_mon_hoc');
    }
    public function giao_vien()
    {
    	return $this -> belongsTo('App\Models\GiaoVien','ma_giao_vien');
    }
   
}
