<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SinhVien extends Model
{
    protected $table = 'sinh_vien';
    protected $fillable = [
    	'ho_ten',
    	'gioi_tinh',
    	'ngay_sinh',
    	'dia_chi',
    	'so_dien_thoai',
    	'email',
    	'ma_lop'
    ];
    public $timestamps = false;
    protected $primaryKey = 'ma';

    public function lop()
    {
    	return $this -> belongsTo('App\Models\Lop','ma_lop');
    }

    public function getTenGioiTinhAttribute($gioi_tinh)
    {
        if ($this -> gioi_tinh == 0){
            return "Nam";
        }
        else{
            return "Nữ";
        }
    }
}
