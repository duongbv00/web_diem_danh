<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TaiKhoan extends Model
{
   protected $table = "tai_khoan";
   protected $fillable = [
   		'ho_ten',
   		'email',
   		'password',
   		'cap_do'
   ];
   public $timestamps = false;
   protected $primaryKey = 'ma';
}
