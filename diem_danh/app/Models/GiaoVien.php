<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GiaoVien extends Model
{
    protected $table = 'giao_vien';
    protected $fillable = [
    	'ho_ten',
    	'email',
    	'password'
    ];
    public $timestamps = false;
    protected $primaryKey = 'ma';
}
