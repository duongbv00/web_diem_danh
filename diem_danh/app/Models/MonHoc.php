<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MonHoc extends Model
{
    protected $table = 'mon_hoc';
    protected $fillable = [
    	'ten_mon_hoc'
    ];
    public $timestamps = false;
    protected $primaryKey = 'ma';
}
