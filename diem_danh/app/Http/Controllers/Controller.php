<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\GiaoVien;
use App\Models\GiaoVu;


use Exception;
use Session;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public function giao_vu()
    {
        return view('giao_vu');
    }
    public function view_login()
    {
    	return view('view_login');
    }
    public function giao_vien()
    {
        return view('giao_vien');
    }

    public function process_login(Request $rq)
    {	
        try {
            $giao_vu = GiaoVu::where('email',$rq -> email)
            -> where('password',$rq -> password) -> firstOrFail();

            Session::put('ma_giao_vu',$giao_vu -> ma);
            Session::put('ho_ten',$giao_vu -> ho_ten);

            return redirect() -> route('giao_vu');
            
        } catch (Exception $e) {

            try {
                $giao_vien = GiaoVien::where('email',$rq -> email)
                -> where('password',$rq -> password) -> first();

                Session::put('ma_giao_vien',$giao_vien -> ma);
                Session::put('ho_ten',$giao_vien -> ho_ten);
                return redirect() -> route('giao_vien');

            } catch (Exception $e) {
                 return redirect() -> route('view_login') -> with('erro','Tài khoản hoặc mật khẩu không chính xác.');
            }
        }

    }

    public function logout()
    {
        Session::flush();
        return redirect() -> route('view_login');
    }
}
