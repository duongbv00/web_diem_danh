<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Lop;
use App\Models\MonHoc;
use App\Models\GiaoVien;
use App\Models\PhanCong;
use Session;


class PhanCongController extends Controller
{
    public function view_all()
    {
    	$array_phan_cong = PhanCong::with('lop','mon_hoc','giao_vien') -> get();

    	return view('phan_cong.view_all',compact('array_phan_cong'));
    }
    public function gv_phan_cong($value='')
    {
        $array_phan_cong = PhanCong::with('lop','mon_hoc','giao_vien') 
        -> where('ma_giao_vien',Session::get('ma_giao_vien'))
        -> get();

        return view('phan_cong.gv_phan_cong',compact('array_phan_cong'));
    }
    public function view_insert()
    {
    	$lop = Lop::get();
    	$giao_vien = GiaoVien::get();
    	$mon_hoc = MonHoc::get();
    	return view('phan_cong.view_insert',compact('lop','giao_vien','mon_hoc'));
    }
    public function process_insert(Request $rq)
    {
    	PhanCong::updateOrCreate([
            'ma_lop' => $rq -> ma_lop,
            'ma_mon_hoc' => $rq -> ma_mon_hoc
        ],[
            'ma_giao_vien' => $rq -> ma_giao_vien
        ]);
        return redirect('phan_cong');
    }
    
    public function delete($ma)
    {
    	PhanCong::destroy($ma);
    	return redirect('phan_cong');
    }
}
