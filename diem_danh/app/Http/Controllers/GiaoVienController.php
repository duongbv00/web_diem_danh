<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\GiaoVien;
use Illuminate\Support\Facades\DB;


class GiaoVienController extends Controller
{
    public function view_all()
    {
    	$array_giao_vien = GiaoVien::get();
    	return view('giao_vien.view_all',compact('array_giao_vien'));
    }
    public function view_insert()
    {
    	return view('giao_vien.view_insert');
    }
    public function process_insert(Request $rq)
    {   
        GiaoVien::create($rq -> all());
    	return redirect('giao_vien');
    }
    public function view_update($ma,Request $rq)
    {
    	$giao_vien = GiaoVien::find($ma);
    	return view('giao_vien.view_update',compact('giao_vien'));
    }
    public function update($ma,Request $rq)
    {
    	GiaoVien::find($ma) -> update($rq -> all());
    	return redirect('GiaoVien');
    }
    public function delete($ma)
    {
    	GiaoVien::destroy($ma);
    	return redirect('GiaoVien');
    }
}
