<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\NganhHoc;

class NganhHocController extends Controller
{
     public function view_all()
    {
    	$array_nganh = NganhHoc::get();
    	return view('nganh_hoc.view_all',compact('array_nganh'));
    }
    public function view_insert()
    {
    	return view('nganh_hoc.view_insert');
    }
    public function process_insert(Request $rq)
    {
    	NganhHoc::create($rq -> all());
    	return redirect('nganh_hoc');
    }
    public function view_update($ma,Request $rq)
    {
    	$nganh_hoc = NganhHoc::find($ma);
    	return view('nganh_hoc.view_update',compact('nganh_hoc'));
    }
    public function update($ma,Request $rq)
    {
    	NganhHoc::find($ma) -> update($rq -> all());
    	return redirect('nganh_hoc');
    }
    public function delete($ma)
    {
    	NganhHoc::destroy($ma);
    	return redirect('nganh_hoc');
    }
}
