<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PhanCong;
use App\Models\GiaoVien;
use App\Models\Lop;
use App\Models\SinhVien;
use App\Models\DiemDanh;
use App\Models\DiemDanhChiTiet;

use Session;
use Exception;


class DiemDanhController extends Controller
{
    public function view_all()
    {
    	$array_lop = PhanCong::where ('ma_giao_vien',Session::get('ma_giao_vien'))
    	-> join('lop','lop.ma','phan_cong_day.ma_lop')
    	-> select('lop.ma','lop.ten_lop')
    	-> distinct()
    	->get();
    	$array_mon = PhanCong::where ('ma_giao_vien',Session::get('ma_giao_vien'))
    	-> join('mon_hoc','mon_hoc.ma','phan_cong_day.ma_mon_hoc')
    	-> select('mon_hoc.ma','mon_hoc.ten_mon_hoc')
    	-> distinct()
    	->get();

    	return view('diem_danh.view_all',compact('array_lop','array_mon'));
    }

    public function view_diem_danh(Request $rq)
    {
        $ma_lop = $rq -> ma_lop;
        $ma_mon_hoc = $rq -> ma_mon_hoc;

        try {
            PhanCong::where('ma_lop',$ma_lop )
            -> where ('ma_mon_hoc',$ma_mon_hoc)
            -> where ('ma_giao_vien',Session::get('ma_giao_vien')) -> firstOrFail();
        } catch (Exception $e) {
            return redirect() -> back() -> with('erro','không có quyền truy cập');
        }

        $array_lop = PhanCong::where ('ma_giao_vien',Session::get('ma_giao_vien'))
        -> join('lop','lop.ma','phan_cong_day.ma_lop')
        -> select('lop.ma','lop.ten_lop')
        -> distinct()
        ->get();
        $array_mon = PhanCong::where ('ma_giao_vien',Session::get('ma_giao_vien'))
        -> join('mon_hoc','mon_hoc.ma','phan_cong_day.ma_mon_hoc')
        -> select('mon_hoc.ma','mon_hoc.ten_mon_hoc')
        -> distinct()
        ->get();

        $array_sinh_vien = SinhVien::where('sinh_vien.ma_lop', $rq -> ma_lop)
        -> get();
        $array_diem_danh = DiemDanh::where('ma_lop', $rq -> ma_lop)
        ->join('diem_danh_chi_tiet','diem_danh_chi_tiet.ma_diem_danh','diem_danh.ma')
        ->where('ma_mon_hoc', $rq -> ma_mon_hoc)
        ->where('ngay_diem_danh',date('Y-m-d'))
        ->select ('diem_danh_chi_tiet.ma_sinh_vien','tinh_trang_di_hoc')
        -> get();

        $array = [];
        foreach ($array_diem_danh as $diem_danh) {
           $array[$diem_danh -> ma_sinh_vien ] = $diem_danh -> tinh_trang_di_hoc;
        }
        return view('diem_danh.view_diem_danh',compact('array_sinh_vien','array_lop','array_mon','ma_lop','ma_mon_hoc','array'));
    }

    public function process_diem_danh(Request $rq)
    {
        $ma_diem_danh = DiemDanh::firstOrCreate([
            'ma_giao_vien' => Session::get('ma_giao_vien'),
            'ma_lop' => $rq -> ma_lop,
            'ma_mon_hoc' => $rq -> ma_mon_hoc,
            'ngay_diem_danh' => date('Y-m-d'),
            'thoi_gian_bat_dau' => $rq -> thoi_gian_bat_dau,
            'thoi_gian_ket_thuc' => $rq -> thoi_gian_ket_thuc,
        ]) -> ma;
        
       foreach ($rq -> tinh_trang_di_hoc as $ma_sinh_vien => $tinh_trang_di_hoc) 
        {
           DiemDanhChiTiet::updateOrCreate([
                'ma_diem_danh' => $ma_diem_danh,
                'ma_sinh_vien' => $ma_sinh_vien,
            ],[
                'tinh_trang_di_hoc' => $tinh_trang_di_hoc
            ]);
        }
    }
}
