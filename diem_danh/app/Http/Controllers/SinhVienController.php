<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\SinhVien;
use App\Models\Lop;
use App\Models\NganhHoc;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\SinhVienImport;


class SinhVienController extends Controller
{
    public function view_all()
    {
        $array_nganh = NganhHoc::get();
    	$array_sinh_vien = DB::table('lop')
    	-> join('sinh_vien','sinh_vien.ma_lop','=','lop.ma')
    	-> join('nganh_hoc','nganh_hoc.ma','=','lop.ma_nganh_hoc')
    	-> join('khoa','khoa.ma','=','lop.ma_khoa')
    	-> get();
    	return view('sinh_vien.view_all',compact('array_sinh_vien','array_nganh'));
    }

    public function ajax(Request $rq)
    {
        $ma_nganh_hoc = $rq -> get('ma_nganh');
        $array_lop = Lop::where('ma_nganh_hoc', $ma_nganh_hoc )
        ->join('khoa','khoa.ma','lop.ma_khoa') 
        ->select('lop.ma','ten_lop','khoa.ten_khoa')
        -> get();
        return $array_lop;
    }

    public function ajax_lop(Request $rq)
    {
        $ma_lop = $rq -> get('ma_lop');
        $array_sinh_vien = SinhVien::where('ma_lop', $ma_lop ) -> get();
        return $array_sinh_vien;
    }

    public function view_insert()
    {
        $lop = Lop::get();
    	return view('sinh_vien.view_insert',compact('lop'));
    }

    public function insert_excel()
    {
        return view('sinh_vien.insert_excel');
    }
    public function process_insert(Request $rq)
    {
    	SinhVien::create($rq -> all());
    	return redirect('sinh_vien');
    }
    public function process_excel(Request $rq)
    {
        Excel::import(new SinhVienImport, $rq -> file_excel);
        return redirect('sinh_vien');
    }
    public function view_update($ma, Request $rq)
    {
        $lop = Lop::get();
    	$sinh_vien = SinhVien::find($ma);
    	return view('sinh_vien.view_update',compact('sinh_vien','lop'));
    }
    public function update($ma,Request $rq)
    {
    	SinhVien::find($ma) -> update($rq -> all());
    	//return redirect('sinh_vien');
    }
    public function delete($ma)
    {
    	SinhVien::destroy($ma);
    	return redirect('sinh_vien');
    }
}
