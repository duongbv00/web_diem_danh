<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Khoa;
class KhoaController extends Controller
{
    public function view_all()
    {
    	$array_khoa = Khoa::get();
    	return view('khoa.view_all',compact('array_khoa'));
    }
    public function view_insert()
    {
    	return view('khoa.view_insert');
    }
    public function process_insert(Request $rq)
    {
    	khoa::create($rq -> all());
    	return redirect('khoa');
    }
    public function view_update($ma,Request $rq)
    {
    	$khoa = khoa::find($ma);
    	return view('khoa.view_update',compact('khoa'));
    }
    public function update($ma,Request $rq)
    {
    	khoa::find($ma) -> update($rq -> all());
    	return redirect('khoa');
    }
    public function delete($ma)
    {
    	khoa::destroy($ma);
    	return redirect('khoa');
    }
}
