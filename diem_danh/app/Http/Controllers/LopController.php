<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Lop;
use App\Models\Khoa;
use App\Models\NganhHoc;

class LopController extends Controller
{
    public function view_all()
    {
    	$array_lop = Lop::with('nganh_hoc','khoa') -> get();
    	return view('lop.view_all',compact('array_lop'));
    }
    public function view_insert()
    {
    	$array_nganh = NganhHoc::get();
    	$array_khoa = Khoa::orderByDesc('ma','Desc') -> get();
    	return view('lop.view_insert',compact('array_nganh','array_khoa'));
    }
    public function process_insert(Request $rq)
    {
    	Lop::create($rq -> all());
    	return redirect('lop');
    }
    public function view_update($ma,Request $rq)
    {
    	$lop = Lop::find($ma);
    	$array_nganh = NganhHoc::get();
    	$array_khoa = Khoa::get();
    	return view('lop.view_update',compact('lop','array_nganh','array_khoa'));
    }
    public function update($ma,Request $rq)
    {
    	Lop::find($ma) -> update($rq -> all());
    	return redirect('lop');
    }
    public function delete($ma)
    {
    	lop::destroy($ma);
    	return redirect('lop');
    }
}
