<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\MonHoc;

class MonHocController extends Controller
{
    public function view_all()
    {
    	$array_mon_hoc = MonHoc::get();
    	return view('mon_hoc.view_all',compact('array_mon_hoc'));
    }
    public function view_insert()
    {
    	return view('mon_hoc.view_insert');
    }
    public function process_insert(Request $rq)
    {
    	MonHoc::create($rq -> all());
    	return redirect('mon_hoc');
    }
    public function view_update($ma,Request $rq)
    {
    	$mon_hoc = MonHoc::find($ma);
    	return view('mon_hoc.view_update',compact('mon_hoc'));
    }
    public function update($ma,Request $rq)
    {
    	MonHoc::find($ma) -> update($rq -> all());
    	return redirect('mon_hoc');
    }
    public function delete($ma)
    {
    	MonHoc::destroy($ma);
    	return redirect('mon_hoc');
    }}
