<?php

namespace App\Http\Middleware;
use Session;
use Closure;

class CheckLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Session::has('ma_giao_vu') && !Session::has('ma_giao_vien')) {
            return redirect() -> route('view_login');
        }
        return $next($request);
    }
}
