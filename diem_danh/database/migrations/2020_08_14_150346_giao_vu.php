<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GiaoVu extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('giao_vu',function(Blueprint $table){
            $table -> increments('ma');
            $table -> string('ho_ten',50);
            $table -> boolean('gioi_tinh');
            $table -> date('ngay_sinh');
            $table -> text('dia_chi');
            $table -> text('email') -> unique();
            $table -> text('password');
            $table -> text('anh');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
