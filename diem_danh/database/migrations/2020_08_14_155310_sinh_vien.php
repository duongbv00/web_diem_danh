<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SinhVien extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema:: create('sinh_vien',function(Blueprint $table){
            $table -> increments('ma');
            $table -> string('ho_ten',50);
            $table -> boolean('gioi_tinh');
            $table -> date('ngay_sinh');
            $table -> text('dia_chi');
            $table -> char('so_dien_thoai');
            $table -> text('email');
            $table -> integer('ma_lop') -> unsigned();

            $table -> foreign('ma_lop') -> references('ma') -> on('lop')-> onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
