<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DiemDanh extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema:: create('diem_danh',function(Blueprint $table){
            $table -> increments('ma');
            $table -> integer('ma_giao_vien') -> unsigned();
            $table -> integer('ma_lop') -> unsigned();
            $table -> integer('ma_mon_hoc') -> unsigned();
            $table -> date('ngay_diem_danh');
            $table -> time('thoi_gian_bat_dau');
            $table -> time('thoi_gian_ket_thuc');

            $table -> foreign('ma_giao_vien') -> references('ma') -> on('giao_vien')-> onDelete('cascade');
            $table -> foreign(['ma_lop','ma_mon_hoc']) -> references(['ma_lop','ma_mon_hoc']) -> on('phan_cong_day')-> onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
