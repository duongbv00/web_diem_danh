<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PhanCongDay extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema:: create('phan_cong_day',function(Blueprint $table){
            $table -> integer('ma_lop') -> unsigned();
            $table -> integer('ma_mon_hoc') -> unsigned();
            $table -> integer('ma_giao_vien') -> unsigned();

            $table -> foreign('ma_lop') -> references('ma') -> on('lop')-> onDelete('cascade');
            $table -> foreign('ma_mon_hoc') -> references('ma') -> on('mon_hoc')-> onDelete('cascade');
            $table -> foreign('ma_giao_vien') -> references('ma') -> on('giao_vien')-> onDelete('cascade');
            $table -> primary(['ma_lop','ma_mon_hoc']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
