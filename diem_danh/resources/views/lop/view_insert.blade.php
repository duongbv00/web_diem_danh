@extends('layout.master.master')

@section('content')

<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<h3>Thêm</h3>
	<form method="POST" action="{{ route('lop.process_insert') }}">
		{{ csrf_field() }}
		Nhập tên lớp
		<input type="text" name="ten_lop">	
		<br>
		Tên khối
		<select name="ma_khoa">
			@foreach ($array_khoa as $khoa)
				<option value="{{ $khoa -> ma }}">
					{{ $khoa -> ten_khoa }}
				</option>
			@endforeach
		</select>
		<br>
		Tên ngành
		<select name="ma_nganh_hoc">
			@foreach ($array_nganh as $nganh)
				<option value="{{ $nganh -> ma }}">
					{{ $nganh -> ten_nganh_hoc }}
				</option>
			@endforeach
		</select>
		<br>	
		<button>Thêm</button>
	</form>
</body>
</html>
@endsection