@extends('layout.master.master')

@section('content')

<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<h3>sửa thông tin</h3>
	<form method="POST" action="{{ route('lop.update',['ma' => $lop -> ma]) }}">
		{{ csrf_field() }}
		tên lớp
		<input type="text" name="ten_nganh_hoc" value="{{ $lop -> ten_lop }}">
		<br>
		Tên Khối
		<select name="ma_khoa">
			@foreach ($array_khoa as $khoa)
				<option value="{{ $khoa -> ma }}" 
				@if ($lop -> ma_khoa == $khoa -> ma)
					selected
				@endif>
					{{ $khoa -> ten_khoa }}
				</option>
			@endforeach
		</select>
		<br>
		Tên ngành
		<select name="ma_nganh_hoc">
			@foreach ($array_nganh as $nganh)
				<option value="{{ $nganh -> ma }}"
					@if ($lop -> ma_nganh_hoc == $nganh -> ma )
						selected 
					@endif>
					{{ $nganh -> ten_nganh_hoc }}
				</option>
			@endforeach
		</select>
		<br>
		<button>sửa</button>
	</form>
</body>
</html>
@endsection