@extends('layout.master.master')

@section('content')

<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<a href="{{ route('lop.view_insert') }}">Thêm lớp học</a>
	<table class="table">
		<tr>
			<th>Mã lớp</th>
			<th>Tên lớp</th>
			<th>Khối</th>
			<th>ngành</th>
			<th>Xửa</th>
			<th>Xóa</th>
		</tr>
		@foreach ($array_lop as $lop)
			<tr>
				<td>
					{{ $lop -> ma }}
				</td>
				<td>
					{{ $lop -> ten_lop }}
				</td>
				<td>
					{{ $lop -> khoa -> ten_khoa }}
				</td>
				<td>
					{{ $lop -> nganh_hoc -> ten_nganh_hoc }}
				</td>
				<td>
					<a href="{{ route('lop.view_update',['ma' => $lop -> ma]) }}">Xửa</a>
				</td>
				<td>
					<a href="{{ route('lop.delete',['ma' => $lop -> ma]) }}">Xóa</a>
				</td>
			</tr>
		@endforeach
	</table>
</body>
</html>
@endsection