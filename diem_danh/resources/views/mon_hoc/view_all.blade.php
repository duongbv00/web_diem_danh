@extends('layout.master.master')

@section('content')

<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<h1>ngành học</h1>
	<a href="{{ route('mon_hoc.view_insert') }}">Thêm khoa học</a>
	<table border="1" width="50%">
		<tr>
			<th>Mã</th>
			<th>Tên môn học</th>
			<th>Xửa</th>
			<th>Xóa</th>
		</tr>
		@foreach ($array_mon_hoc as $mon_hoc)
			<tr>
				<td>
					{{ $mon_hoc -> ma }}
				</td>
				<td>
					{{ $mon_hoc -> ten_mon_hoc }}
				</td>
				<td>
					<a href="{{ route('mon_hoc.view_update',['ma' => $mon_hoc->ma]) }}">Sửa</a>
				</td>
				<td>
					<a href="{{ route('mon_hoc.delete',['ma' => $mon_hoc->ma]) }}">Xóa</a>
				</td>
			</tr>
		@endforeach
	</table>
</body>
</html>
@endsection