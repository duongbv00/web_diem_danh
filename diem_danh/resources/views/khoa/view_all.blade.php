@extends('layout.master.master')

@section('content')

<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<a href="{{ route('khoa.view_insert') }}">Thêm liên khóa</a>
	<table class="table">
		<tr>
			<th>Mã khóa</th>
			<th>Tên khóa</th>
			<th>Xửa</th>
			<th>Xóa</th>
		</tr>
		@foreach ($array_khoa as $khoa)
			<tr>
				<td>
					{{ $khoa -> ma }}
				</td>
				<td>
					{{ $khoa -> ten_khoa }}
				</td>
				<td>
					<a href="{{ route('khoa.view_update',['ma' => $khoa->ma]) }}">Sửa</a>
				</td>
				<td>
					<a href="{{ route('khoa.delete',['ma' => $khoa->ma]) }}">Xóa</a>
				</td>
			</tr>
		@endforeach
	</table>
</body>
</html>
@endsection