<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	@extends('layout.master.master')

	@section('content')
	<form method="POST" action="{{ route('sinh_vien.process_insert') }}" class="custom-validation">
		{{ csrf_field() }}
		<h3 style="text-align: center;">Thêm sinh viên</h3>
		<br>
		<div class="form-group">
			<label>Họ Tên</label>
			<input class="form-control" type="text" name="ho_ten" >
		</div>

		<div class="form-group">	
			<label>Giới Tính</label> &nbsp;&nbsp;&nbsp;
			<input class="" type="radio" name="gioi_tinh" value ="0">&nbsp;Nam&nbsp;&nbsp;
			<input class="" type="radio" name="gioi_tinh" value ="1">&nbsp;Nữ
		</div>

		<div class="form-group">
			<label>Ngày Sinh</label>
			<input class="form-control" type="date" name="ngay_sinh">
		</div>

		<div class="form-group">
			<label>Địa Chỉ</label>
			<input class="form-control" type="text" name="dia_chi">	
		</div>

		<div class="form-group">
			<label>Số Điện Thoại</label>
			<input class="form-control" type="text" name="so_dien_thoai">
		</div>

		<div class="form-group">	
			<label>Email</label>
			<input class="form-control" type="email" name="email">
		</div>
		<div class="form-group">	
			<label>lớp</label>
			<select name = "ma_lop" class="form-control">
				<option selected >--Chọn--</option>
				@foreach ($lop as $lop)
					<option value="{{ $lop -> ma }}">
						{{ $lop -> ten_lop }}
					</option>
				@endforeach
			</select>
		</div>
		<button class="btn btn-primary waves-effect waves-light mr-1">Thêm</button>
	</form>
	@endsection
</body>
</html>