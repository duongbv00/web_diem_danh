@extends('layout.master.master')

@section('content')
<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<h3>Thêm Sinh Viên Bằng File Excel</h3>
	<br>
	<form method="POST" enctype="multipart/form-data" action="{{ route('sinh_vien.process_excel') }}">
		{{ csrf_field() }}
		Chọn file exel:
		<input type="file" name="file_excel"  accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" class="dz-default dz-message">
		<br>
		<br>
		<button class="btn btn-primary waves-effect waves-light">Thêm</button>
	</form>
</body>
</html>
@endsection