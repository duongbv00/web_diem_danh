<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	@extends('layout.master.master')

	@section('content')
	<h3>sửa thông tin</h3>
	<form method="POST" action="{{ route('sinh_vien.update',['ma' => $sinh_vien -> ma]) }}" class="table">
		{{ csrf_field() }}
		<table >
			<tr>
				<th>Họ tên</th>
				<th>Giới tính</th>
				<th>Ngày sinh</th>
				<th>Đỉa chỉ</th>
				<th>Số điện thoại</th>
				<th>email</th>
				<th>Lớp</th>
			</tr>
			<tr>
				<td>
					<input type="text" name="ho_ten" value="{{ $sinh_vien -> ho_ten }}">
				</td>
				<td>
					<input type="radio" name="gioi_tinh" value ="0" 
					@if ($sinh_vien -> gioi_tinh == 0)
						checked 
					@endif>Nam &ensp;
					<input type="radio" name="gioi_tinh" value ="1" 
					@if ($sinh_vien -> gioi_tinh ==1)
						checked 
					@endif
					>Nữ
				</td>
				<td>
					<input type="date" name="ngay_sinh" value="{{ $sinh_vien -> ngay_sinh }}">
				</td>
				<td>
					<input type="text" name="so_dien_thoai" value="{{ $sinh_vien -> so_dien_thoai }}">
				</td>
				<td>
					<input type="text" name="dia_chi" value="{{ $sinh_vien -> dia_chi }}">
				</td>
				<td>
					<input type="email" name="email" value="{{ $sinh_vien -> email }}">
				</td>
				<td>
					<select name = "ma_lop">
					@foreach ($lop as $array_lop)
						<option value="{{ $array_lop -> ma}}" 
							@if ($sinh_vien -> ma  == $array_lop -> ma )
							selected
						@endif>{{ $array_lop -> ten_lop }}</option>
					@endforeach
					</select>
				</td>
			</tr>
		</table>
		<button>sửa</button>
	</form>
@endsection
</body>
</html>