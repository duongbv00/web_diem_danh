@extends('layout.master.master')

@section('content')

<!DOCTYPE html>
<html>
<head>
	<title></title>
	<script type="text/javascript" src="{{ asset('js/jquery.js') }}"></script>
	<script type="text/javascript">
		jQuery(document).ready(function($) {
			$('#nganh_hoc').change(function() {
				var ma_nganh_hoc = $(this).val();
				$.ajax({
					url: '{{ route('sinh_vien.ajax') }}',
					type: 'GET',
					dataType: 'json',
					data: {ma_nganh:ma_nganh_hoc},
				})
				.done(function(response) {
					$("#lop").html('');
					$("#lop").append('<option selected disabled >---Chọn---</option>');
					$(response).each(function() {
						$("#lop").append(`
							<option value='${this.ma}'> ${this.ten_lop} ${this.ten_khoa	} </option>
						`);
					});
				})
				.fail(function() {
					console.log("error");
				})
			});
			$('#lop').change(function() {
				var ma_lop = $(this).val();
				$.ajax({
					url: '{{ route('sinh_vien.ajax_lop') }}',
					type: 'GET',
					dataType: 'json',
					data: {ma_lop: ma_lop},
				})
				.done(function(response) {
					$("#sinh_vien").html('');
					$("#sinh_vien").append(`
						<tr>
							<th>Họ tên</th>
							<th>Giới tính</th>
							<th>Ngày sinh</th>
							<th>Đỉa chỉ</th>
							<th>Số điện thoại</th>
							<th>Email</th>
							<th>Sửa</th>
							<th>Xóa</th>
						</tr>
					`);
					$(response).each(function() {
						$("#sinh_vien").append(`
								<tr>
									<td>
										${this.ho_ten}
									</td>
									<td>
										${this.gioi_tinh}
									</td>
									<td>
										${this.ngay_sinh}
									</td>
									<td>
										${this.dia_chi}
									</td>
									<td>
										${this.so_dien_thoai}
									</td>
									<td>
										${this.email}
									</td>
								</tr>
						`);
					});
				})
				.fail(function() {
					console.log("error");
				})
			});
		});
	</script>
</head>
<body>
	<h3>Danh Sách Sinh Viên</h3>
<div>
	<div class="form-group">	
		<label>Ngành Học</label>
		<select name ="ma_nganh_hoc" class="form-control" id="nganh_hoc" style="width: 30%;">
			<option disabled selected >
				<span>---Chọn---</span>
			</option>
			@foreach ($array_nganh as $nganh)
				<option value="{{ $nganh -> ma }}">
					{{ $nganh -> ten_nganh_hoc }}
				</option>
			@endforeach
		</select>
	</div>
	<div class="form-group">	
		<label>lớp</label>
		<select name = "ma_lop" class="form-control" id="lop" style="width: 30%;">
			<option selected disabled >---Chọn---</option>
		</select>
	</div>
</div>
<br>
<table class=" table" id="sinh_vien">
	{{-- <tr>
		<th>Họ tên</th>
		<th>Giới tính</th>
		<th>Ngày sinh</th>
		<th>Đỉa chỉ</th>
		<th>Số điện thoại</th>
		<th>Email</th>
		<th>Sửa</th>
		<th>Xóa</th>
	</tr>
	@foreach ($array_sinh_vien as $sinh_vien)
		<tr>
			<td>
				{{ $sinh_vien -> ho_ten }}
			</td>
			<td>
				{{ $sinh_vien -> gioi_tinh }}
			</td>
			<td>
				{{ $sinh_vien -> ngay_sinh }}
			</td>
			<td>
				{{ $sinh_vien -> dia_chi }}
			</td>
			<td>
				{{ $sinh_vien -> so_dien_thoai }}
			</td>
			<td>
				{{ $sinh_vien -> email }}
			</td>
			<td>
				{{ $sinh_vien -> ten_lop }}{{ $sinh_vien -> ten_khoa }}
			</td>
			<td>
				{{ $sinh_vien -> ten_nganh_hoc }}
			</td>
			<td>
				<a href="{{ route('sinh_vien.view_update',['ma' => $sinh_vien -> ma]) }}">Sửa</a>
			</td>
		</tr>
	@endforeach --}}
</table>
</body>
</html>
@endsection