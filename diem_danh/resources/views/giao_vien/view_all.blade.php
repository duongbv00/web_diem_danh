<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	@extends('layout.master.master')

	@section('content')
	<h1>Danh sách giáo viên</h1>
	<a href="{{ route('GiaoVien.view_insert') }}">Đăng kí tài khoản giáo viên</a>
	<table border="1" width="50%" class="table">
		<tr>
			<th>Họ tên</th>
			<th>Giới tính</th>
			<th>Ngày sinh</th>
			<th>Địa chỉ</th>
			<th>Số điện thoại</th>
			<th>Email</th>
			<th>Xửa</th>
			<th>Xóa</th>
		</tr>
		@foreach ($array_giao_vien as $giao_vien)
			<tr>
				<td>
					{{ $giao_vien -> ho_ten }}
				</td>
				<td>
					{{ $giao_vien -> gioi_tinh }}
				</td>
				<td>
					{{ $giao_vien -> ngay_sinh }}
				</td>
				<td>
					{{ $giao_vien -> que_quan}}
				</td>
				<td>
					{{ $giao_vien -> so_dien_thoai }}
				</td>
				<td>
					{{ $giao_vien -> email }}
				</td>
				<td>
					<a href="{{ route('GiaoVien.view_update',['ma' => $giao_vien->ma]) }}">Sửa</a>
				</td>
				<td>
					<a href="{{ route('GiaoVien.delete',['ma' => $giao_vien->ma]) }}">Xóa</a>
				</td>
			</tr>
		@endforeach
	</table>
@endsection
</body>
</html>