@extends('layout.master.master')

@section('content')
<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<h3>Đăng Kí Tài Khoản Cho Giáo Viên</h3>
	<br>
	<form method="POST" action="{{ route('GiaoVien.process_insert') }}">
		{{ csrf_field() }}
		Họ tên
		<input type="text" name="ho_ten">
		<br>
		Email
		<input type="email" name="email">
		<br>
		Password
		<input type="password" name="password">
		<br>
		<button>Đăng kí</button>
	</form>
</body>
</html>
@endsection