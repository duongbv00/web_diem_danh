@extends('layout.master.master-gv')

@section('content')

<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	@if (Session::has('error'))
		<h1>{{ Session::get('error') }}</h1>
	@endif
	<form  method="POST" {{ route('diem_danh.view_diem_danh') }}>
		{{ csrf_field() }}
		Chon lớp
		<select name="ma_lop">
			<option>--Chọn--</option>
			@foreach ($array_lop as $lop)
				<option value="{{ $lop -> ma }}"> {{ $lop -> ten_lop }}</option>
			@endforeach
		</select>
		<br>
		chon mon
		<select name="ma_mon_hoc">
			<option>--Chọn--</option>
			@foreach ($array_mon as $mon_hoc)
				<option value="{{ $mon_hoc -> ma }}"> {{ $mon_hoc -> ten_mon_hoc }} </option>
			@endforeach
		</select>
		<button>Chọn</button>
	</form>
</body>
</html>
@endsection