@extends('layout.master.master')

@section('content')
<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	Danh sách phân công
	<br>
	<a href="{{ route('phan_cong.view_insert') }}">Phân công</a>
	<table border="1" width="50%" class="table table-bordered dt-responsive nowrap dataTable no-footer dtr-inline">
		<tr>
			<th>Giáo viên</th>
			<th>Lớp</th>
			<th>Môn học</th>
			<th>Xóa</th>
		</tr>
		@foreach ($array_phan_cong as $phan_cong)
			<tr>
				<td>
					{{ $phan_cong -> giao_vien -> ho_ten }}
				</td>
				<td>
					{{ $phan_cong -> lop -> ten_lop }}
				</td>
				<td>
					{{ $phan_cong -> mon_hoc -> ten_mon_hoc }}
				</td>
				<td>
					<a href="{{ route('phan_cong.delete',['ma' => $phan_cong->ma_lop ]) }}">Xóa</a>
				</td>
			</tr>
		@endforeach
	</table>
</body>
</html>
@endsection