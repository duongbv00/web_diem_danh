@extends('layout.master.master')

@section('content')
<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<form method="POST" action="{{ route('phan_cong.process_insert') }}">
		{{ csrf_field() }}
		tên giáo viên
		<select name="ma_giao_vien">
			@foreach ($giao_vien as $giao_vien)
				<option value="{{ $giao_vien -> ma }}">{{ $giao_vien -> ho_ten }}</option>
			@endforeach
		</select>
		<br>
		<br>
		tên lớp
		<select name="ma_lop">
			@foreach ($lop as $lop)
				<option value="{{ $lop -> ma }}">{{ $lop -> ten_lop }}</option>
			@endforeach
		</select>
		<br>
		tên môn học
		<select name="ma_mon_hoc">
			@foreach ($mon_hoc as $mon_hoc)
				<option value="{{ $mon_hoc -> ma }}" >{{ $mon_hoc -> ten_mon_hoc }}</option>
			@endforeach
		</select>
		<button>Thêm</button>
	</form>
</body>
</html>
@endsection