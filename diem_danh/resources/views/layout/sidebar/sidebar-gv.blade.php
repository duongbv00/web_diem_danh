<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <div id="sidebar-menu">
        <!-- Left Menu Start -->
        <ul class="metismenu list-unstyled" id="side-menu">
            <li class="menu-title">MENU</li>


           <li>
                <a href="javascript: void(0);" class="has-arrow waves-effect">
                    <i class="ti-clipboard"></i>
                    <span> Điểm Danh </span>
                </a>
                <ul class="sub-menu" aria-expanded="false">
                    <li><a href="{{ route('diem_danh.view_all') }}">Điểm Danh</a></li>
                    <li><a href="email-read.html">Lịch Sử Điểm Danh</a></li>
                </ul>
            </li>

            <li>
                <a href="{{ route('phan_cong.gv_phan_cong') }}">
                    <i class="mdi mdi-calendar-check"></i>
                    <span> Lịch Phân Công </span>
                </a>
            </li>
        </ul>
    </div>
</body>
</html>