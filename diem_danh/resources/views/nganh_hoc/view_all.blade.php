@extends('layout.master.master')

@section('content')

<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<h1>ngành học</h1>
	<a href="{{ route('nganh_hoc.view_insert') }}">Thêm ngành học</a>
	<table border="1" width="50%">
		<tr>
			<th>Mã ngành</th>
			<th>Tên ngành</th>
			<th>Xửa</th>
			<th>Xóa</th>
		</tr>
		@foreach ($array_nganh as $nganh)
			<tr>
				<td>
					{{ $nganh -> ma }}
				</td>
				<td>
					{{ $nganh -> ten_nganh_hoc }}
				</td>
				<td>
					<a href="{{ route('nganh_hoc.view_update',['ma' => $nganh->ma]) }}">Sửa</a>
				</td>
				<td>
					<a href="{{ route('nganh_hoc.delete',['ma' => $nganh->ma]) }}">Xóa</a>
				</td>
			</tr>
		@endforeach
	</table>
</body>
</html>
@endsection