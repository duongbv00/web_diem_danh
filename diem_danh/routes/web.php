<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('logout','Controller@logout') -> name('logout');

Route::group(['middleware' => 'CheckLogin'],function()
{
	Route::group(['middleware' => 'CheckGiaoVu'],function()
	{

		Route::group(['frefix' => 'GiaoVien','as' =>'GiaoVien.'],function(){
			Route::get('giao_vien','GiaoVienController@view_all') -> name('view_all');
			Route::get('giao_vien/view_insert','GiaoVienController@view_insert') -> name('view_insert');
			Route::post('giao_vien/process_insert','GiaoVienController@process_insert') -> name('process_insert');
			Route::get('giao_vien/view_update/{ma}','GiaoVienController@view_update') -> name('view_update');
			Route::post('giao_vien/update/{ma}','GiaoVienController@update') -> name('update');
			Route::get('giao_vien/delete/{ma}','GiaoVienController@delete') -> name('delete');
		});

		Route::group(['frefix' => 'nganh_hoc','as' =>'nganh_hoc.'],function(){
			Route::get('nganh_hoc','NganhHocController@view_all') -> name('view_all');
			Route::get('nganh_hoc/view_insert','NganhHocController@view_insert') -> name('view_insert');
			Route::post('nganh_hoc/process_insert','NganhHocController@process_insert') -> name('process_insert');
			Route::get('nganh_hoc/view_update/{ma}','NganhHocController@view_update') -> name('view_update');
			Route::post('nganh_hoc/update/{ma}','NganhHocController@update') -> name('update');
			Route::get('nganh_hoc/delete/{ma}','NganhHocController@delete') -> name('delete');
		});

		Route::group(['frefix' => 'khoa','as' =>'khoa.'],function(){
			Route::get('khoa','KhoaController@view_all') -> name('view_all');
			Route::get('khoa/view_insert','KhoaController@view_insert') -> name('view_insert');
			Route::post('khoa/process_insert','KhoaController@process_insert') -> name('process_insert');
			Route::get('khoa/view_update/{ma}','KhoaController@view_update') -> name('view_update');
			Route::post('khoa/update/{ma}','KhoaController@update') -> name('update');
			Route::get('khoa/delete/{ma}','KhoaController@delete') -> name('delete');
		});

		Route::group(['frefix' => 'mon_hoc','as' =>'mon_hoc.'],function(){
			Route::get('mon_hoc','MonHocController@view_all') -> name('view_all');
			Route::get('mon_hoc/view_insert','MonHocController@view_insert') -> name('view_insert');
			Route::post('mon_hoc/process_insert','MonHocController@process_insert') -> name('process_insert');
			Route::get('mon_hoc/view_update/{ma}','MonHocController@view_update') -> name('view_update');
			Route::post('mon_hoc/update/{ma}','MonHocController@update') -> name('update');
			Route::get('mon_hoc/delete/{ma}','MonHocController@delete') -> name('delete');
		});

		Route::group(['frefix' => 'lop','as' =>'lop.'],function(){
			Route::get('lop','LopController@view_all') -> name('view_all');
			Route::get('lop/view_insert','LopController@view_insert') -> name('view_insert');
			Route::post('lop/process_insert','LopController@process_insert') -> name('process_insert');
			Route::get('lop/view_update/{ma}','LopController@view_update') -> name('view_update');
			Route::post('lop/update/{ma}','LopController@update') -> name('update');
			Route::get('lop/delete/{ma}','LopController@delete') -> name('delete');
		});

		Route::group(['frefix' => 'sinh_vien','as' =>'sinh_vien.'],function(){
			Route::get('sinh_vien','SinhVienController@view_all') -> name('view_all');
			Route::get('ajax','SinhVienController@ajax') -> name('ajax');
			Route::get('ajax_lop','SinhVienController@ajax_lop') -> name('ajax_lop');
			Route::get('excel','SinhVienController@insert_excel') -> name('insert_excel');
			Route::get('sinh_vien/view_insert','SinhVienController@view_insert') -> name('view_insert');
			Route::post('sinh_vien/process_insert','SinhVienController@process_insert') -> name('process_insert');
			Route::post('sinh_vien/excel','SinhVienController@process_excel') -> name('process_excel');
			Route::get('sinh_vien/view_update/','SinhVienController@view_update') -> name('view_update');
			Route::post('sinh_vien/update/{ma}','SinhVienController@update') -> name('update');
			Route::get('sinh_vien/delete/{ma}','SinhVienController@delete') -> name('delete');
		});
		Route::group(['frefix' => 'tai_khoan','as' =>'tai_khoan.'],function(){
			Route::get('tai_khoan','TaiKhoanController@thong_tin') -> name('thong_tin');
		});
	});

	Route::group(['frefix' => 'phan_cong','as' =>'phan_cong.'],function(){
		Route::get('phan_cong','PhanCongController@view_all') -> name('view_all') ->middleware('CheckGiaoVu');
		Route::get('phan_cong_gv','PhanCongController@gv_phan_cong') -> name('gv_phan_cong') ->middleware('CheckGiaoVien');
		Route::get('phan_cong/view_insert','PhanCongController@view_insert') -> name('view_insert') -> middleware('CheckGiaoVu');
		Route::post('phan_cong/process_insert','PhanCongController@process_insert') -> name('process_insert') ->middleware('CheckGiaoVu');
		Route::get('phan_cong/view_update/{ma}','PhanCongController@view_update') -> name('view_update') ->middleware('CheckGiaoVu');
		Route::post('phan_cong/update/{ma}','PhanCongController@update') -> name('update') -> middleware('CheckGiaoVu');
		Route::get('phan_cong/delete/{ma}','PhanCongController@delete') -> name('delete') -> middleware('CheckGiaoVu');
	});

	Route::group(['frefix' => 'diem_danh','as' =>'diem_danh.'],function(){
		Route::get('diem_danh','DiemDanhController@view_all') -> name('view_all');
		Route::post('diem_danh','DiemDanhController@view_diem_danh') -> name('view_diem_danh');
		Route::get('view_insert','DiemDanhController@view_insert') -> name('view_insert');
		Route::post('process_diem_danh','DiemDanhController@process_diem_danh') -> name('process_diem_danh');
		Route::get('view_update/{ma}','DiemDanhController@view_update') -> name('view_update');
		Route::post('update/{ma}','DiemDanhController@update') -> name('update');
		Route::get('delete/{ma}','DiemDanhController@delete') -> name('delete');
	});


	Route::get('giao_vu','Controller@giao_vu') -> name('giao_vu') -> middleware('CheckGiaoVu');
	Route::get('trang_giao_vien','Controller@giao_vien') -> name('giao_vien')-> middleware('CheckGiaoVien');
});
Route::get('','Controller@view_login')->name('view_login');
Route::post('process_login','Controller@process_login') -> name('process_login');


